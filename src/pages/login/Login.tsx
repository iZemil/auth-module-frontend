import * as React from 'react';
import { useForm } from 'react-hook-form';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { AuthForm } from '../../components/AuthForm';
import { Button, Input, Link } from '../../components/ui';
import { REG_EXPS } from '../../utils/constants/regExps';
import { ROUTES } from '../../routes';
import { authActions } from '../../store/modules/auth/actions';

type TInputs = {
    email: string;
    password: string;
};

const mapDispatchToProps = { login: authActions.login };

type TLoginComponentProps = typeof mapDispatchToProps;

export function LoginComponent(props: TLoginComponentProps) {
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm<TInputs>();
    const onSubmit = async (data: TInputs) => {
        await props.login(data);

        history.push(ROUTES.PROTECTED.path);
    };

    return (
        <AuthForm title="Welcome back!" onSubmit={handleSubmit(onSubmit)}>
            <Input
                type="email"
                label="Email"
                name="email"
                err={errors.email}
                register={register({
                    required: true,
                    pattern: REG_EXPS.email,
                })}
            />
            <Input
                type="password"
                label="Password"
                name="password"
                err={errors.password}
                register={register({ required: true, pattern: REG_EXPS.simplePassword })}
            />
            <Button type="submit">Login</Button>

            <p>
                Need an account? <Link to={ROUTES.SIGN_UP.path}>Register</Link>
            </p>
        </AuthForm>
    );
}

export const Login = connect(null, mapDispatchToProps)(LoginComponent);
