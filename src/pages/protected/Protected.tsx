import { connect } from 'react-redux';
import styled from 'styled-components';

import { Button } from '../../components/ui';
import { authActions } from '../../store/modules/auth/actions';
import { COLORS } from '../../utils/constants/colors';

export const ProtectedPageWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    background: ${COLORS.bg};
    height: 100vh;
    flex-direction: column;

    h1 {
        color: white;
    }
`;

const mapDispatchToProps = { signOut: authActions.signOut };

type TProtectedComponentProps = typeof mapDispatchToProps;

export function ProtectedComponent(props: TProtectedComponentProps) {
    return (
        <ProtectedPageWrapper>
            <h1>Protected Page Router</h1>

            <Button type="button" onClick={() => props.signOut()}>
                Sign out
            </Button>
        </ProtectedPageWrapper>
    );
}

export const Protected = connect(null, mapDispatchToProps)(ProtectedComponent);
