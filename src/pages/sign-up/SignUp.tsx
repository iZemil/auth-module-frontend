import * as React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

import { Button, Input, Link } from '../../components/ui';
import { ROUTES } from '../../routes';
import { AuthForm } from '../../components/AuthForm';
import { REG_EXPS } from '../../utils/constants/regExps';
import { connect } from 'react-redux';
import { authActions } from '../../store/modules/auth/actions';

type TInputs = {
    email: string;
    username: string;
    password: string;
};

const mapDispatchToProps = { signUp: authActions.signUp };

type TSignUpComponentProps = typeof mapDispatchToProps;

export function SignUpComponent(props: TSignUpComponentProps) {
    const history = useHistory();
    const { register, handleSubmit, errors } = useForm<TInputs>();
    const onSubmit = async (data: TInputs) => {
        await props.signUp(data);

        history.push(ROUTES.PROTECTED.path);
    };

    return (
        <AuthForm title="Create an acount" onSubmit={handleSubmit(onSubmit)}>
            <Input
                label="Email"
                name="email"
                register={register({
                    required: true,
                    pattern: REG_EXPS.email,
                })}
                err={errors.email}
            />
            <Input
                label="Username"
                name="username"
                register={register({ required: true, minLength: 4 })}
                err={errors.username}
            />
            <Input
                label="Password"
                type="password"
                name="password"
                register={register({ required: true, pattern: REG_EXPS.simplePassword })}
                err={errors.password}
            />

            <Button type="submit">Join</Button>

            <p>
                <Link to={ROUTES.LOGIN.path}>Already have an account?</Link>
            </p>
        </AuthForm>
    );
}

export const SignUp = connect(null, mapDispatchToProps)(SignUpComponent);
