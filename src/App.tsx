import * as React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';

import { ROUTES } from './routes';
import { DevMenu } from './components/DevMenu';
import { ProtectedRoute } from './components/ProtectedRoute';
import configureStore from './store';

const reduxStore = configureStore();
const routes = Object.values(ROUTES);

export default function App() {
    return (
        <ReduxProvider store={reduxStore}>
            <Router>
                <>
                    <DevMenu />

                    <Switch>
                        {routes.map((it) =>
                            it.isProtected ? (
                                <ProtectedRoute key={it.title} {...it} />
                            ) : (
                                <Route key={it.title} path={it.path} exact={Boolean(it.exact)}>
                                    {it.component}
                                </Route>
                            )
                        )}
                    </Switch>
                </>
            </Router>
        </ReduxProvider>
    );
}
