export const REG_EXPS = {
    email: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
    simplePassword: /[a-zA-Z0-9]{8,}/,
};
