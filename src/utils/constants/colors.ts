export const COLORS = {
    primary: '#7289da',
    bg: '#36393f',
    primaryText: '#dcddde',
    secondaryText: '#8e9297',
    error: '#f04747',
    black: '#000',
};
