import * as React from 'react';
import { RouteProps } from 'react-router-dom';

import { Login } from './pages/login/Login';
import { Protected } from './pages/protected/Protected';
import { SignUp } from './pages/sign-up/SignUp';

interface IRoute extends RouteProps {
    path: string;
    title: string;
    isProtected?: boolean;
}

type TRouteKeys = 'HOME' | 'LOGIN' | 'SIGN_UP' | 'PROTECTED';
type TRoutes = Record<TRouteKeys, IRoute>;

export const ROUTES: TRoutes = {
    HOME: {
        path: '/',
        exact: true,
        component: (props: IRoute) => <Login {...props} />,
        title: 'Home',
    },
    LOGIN: {
        path: '/login',
        component: (props: IRoute) => <Login {...props} />,
        title: 'Login',
    },
    SIGN_UP: {
        path: '/sign-up',
        component: (props: IRoute) => <SignUp {...props} />,
        title: 'Sign Up',
    },
    PROTECTED: {
        path: '/protected',
        component: (props: IRoute) => <Protected {...props} />,
        title: 'Protected Route',
        isProtected: true,
    },
};
