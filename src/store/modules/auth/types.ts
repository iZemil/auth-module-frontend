export const LOGIN = 'auth/LOGIN';
export const LOGIN_COMPLETED = 'auth/LOGIN_COMPLETED';
export const LOGIN_FAILED = 'auth/LOGIN_FAILED';

export const SIGN_IN = 'auth/SIGN_IN';
export const SIGN_IN_COMPLETED = 'auth/SIGN_IN_COMPLETED';
export const SIGN_IN_FAILED = 'auth/SIGN_IN_FAILED';

export const SIGN_OUT = 'auth/SIGN_OUT';
export const SIGN_OUT_COMPLETED = 'auth/SIGN_OUT_COMPLETED';
export const SIGN_OUT_FAILED = 'auth/SIGN_OUT_FAILED';
