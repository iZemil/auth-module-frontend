import { API, REST, REST_PATHS } from '../../middlewares/api/constants';

import * as types from './types';

export interface ILoginPayload {
    email: string;
    password: string;
}

function login({ email, password }: ILoginPayload) {
    return {
        type: API,
        payload: {
            path: REST_PATHS.Login,
            method: REST.Post,
            data: {
                email,
                password,
            },
            onSuccess: () => {
                return { type: types.LOGIN };
            },
            onFailure: (err: Error) => {
                console.log('Error occured: Login', err);
            },
            label: types.LOGIN,
        },
    };
}

export interface ISignUpPayload {
    email: string;
    username: string;
    password: string;
}

function signUp({ email, username, password }: ISignUpPayload) {
    return {
        type: API,
        payload: {
            path: REST_PATHS.SignUp,
            method: REST.Post,
            data: { email, username, password },
            onSuccess: () => {
                return { type: types.SIGN_IN };
            },
            onFailure: (err: Error) => {
                console.log('Error occured: Sign in', err);
            },
            label: types.SIGN_IN,
        },
    };
}

function signOut() {
    return {
        type: API,
        payload: {
            path: REST_PATHS.SignOut,
            method: REST.Post,
            data: null,
            onSuccess: () => {
                return { type: types.SIGN_OUT };
            },
            onFailure: (err: Error) => {
                console.log('Error occured: Sign out', err);
            },
            label: types.SIGN_OUT,
        },
    };
}

export const authActions = {
    login,
    signUp,
    signOut,
};
