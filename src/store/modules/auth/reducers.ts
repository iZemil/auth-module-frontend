import * as types from './types';

export interface IAction {
    type: string;
    payload?: any;
}

export function auth(
    state = {
        isAuthenticated: false,
    },
    action: IAction
) {
    switch (action.type) {
        case types.LOGIN:
            return { ...state, isAuthenticated: true };
        case types.SIGN_OUT:
            return { ...state, isAuthenticated: false };
        case types.SIGN_IN:
            return { ...state, isAuthenticated: true };
        default:
            return state;
    }
}
