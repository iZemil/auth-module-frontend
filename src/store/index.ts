import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import * as reducers from './modules';
import { apiMiddleware } from './middlewares';

const rootReducer = combineReducers(reducers);

export type TRootState = ReturnType<typeof rootReducer>;

export default function configureStore(initialState?: TRootState) {
    return createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware, apiMiddleware)));
}
