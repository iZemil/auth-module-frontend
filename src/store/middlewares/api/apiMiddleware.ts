import axios from 'axios';
import { Dispatch } from 'redux';

import { IAction } from '../../modules/auth/reducers';

import { apiStart, apiEnd, apiError, accessDenied } from './actions';
import { API, API_ERROR, REST } from './constants';

function getApiUrl(path: string): string {
    const apiUrl = process.env.API_URL;
    const mockServerApiUrl = 'http://localhost:8082/api';

    return `${apiUrl ?? mockServerApiUrl}${path}`;
}

export function apiMiddleware({ dispatch }: { dispatch: Dispatch }) {
    return (next: (a: IAction) => void) => async (action: IAction) => {
        next(action);

        if (action.type !== API) {
            return;
        }

        const { url, path, method, data, token, onSuccess, onFailure, label, headers } = action.payload;
        const dataOrParams = [REST.Get, REST.Delete].includes(method) ? 'params' : 'data';

        /* axios default configs */
        axios.defaults.baseURL = process.env.REACT_APP_BASE_URL || '';
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = `Bearer${token}`;

        if (label) {
            dispatch(apiStart(label));
        }

        try {
            const res = await axios.request({
                url: url ?? getApiUrl(path),
                method,
                headers,
                [dataOrParams]: data,
            });
            dispatch(onSuccess(res.data));
        } catch (error) {
            dispatch(apiError(error));
            dispatch(onFailure(error) ?? { type: API_ERROR });

            if (error.response && error.response.status === 403) {
                dispatch(accessDenied(window.location.pathname));
            }
        } finally {
            if (label) {
                dispatch(apiEnd(label));
            }
        }
    };
}
