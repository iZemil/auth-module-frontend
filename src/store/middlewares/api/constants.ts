export const API = 'API';
export const API_START = 'API_START';
export const API_END = 'API_END';
export const ACCESS_DENIED = 'ACCESS_DENIED';
export const API_ERROR = 'API_ERROR';

export const REST = {
    Get: 'GET',
    Post: 'POST',
    Put: 'PUT',
    Patch: 'PATCH',
    Delete: 'DELETE',
};

export const REST_PATHS = {
    Login: '/auth/login',
    SignUp: '/auth/sign-up',
    SignOut: '/auth/sign-out',
};
