import * as React from 'react';

import { IProps } from './types';
import { FormWrapper, FormTitle, Form } from './Styled';

export function AuthForm({ children, title, onSubmit }: IProps) {
    return (
        <FormWrapper>
            <Form autoComplete="off" onSubmit={onSubmit}>
                <FormTitle>{title}</FormTitle>

                {children}
            </Form>
        </FormWrapper>
    );
}
