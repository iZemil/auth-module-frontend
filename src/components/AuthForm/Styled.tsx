import styled from 'styled-components';

export const FormTitle = styled.h1`
    font-size: 1.5em;
    text-align: center;
    color: #fff;
    opacity: 0.95;
`;

export const FormWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    background: #bbbbbb;
    height: 100vh;
`;

export const Form = styled.form`
    color: #72767d;
    background: #36393f;
    border-radius: 4px;
    flex: 1;
    max-width: 400px;
    padding: 24px 20px;
    display: flex;
    flex-direction: column;
    border: 1px solid rgb(0 0 0 / 50%);
    box-shadow: 0 0 4px rgb(0 0 0 / 50%);

    .form-view {
        &__input {
            margin-top: 10px;
            margin-bottom: 20px;
        }

        &__logo {
            width: 100%;
            height: auto;
        }
    }
`;
