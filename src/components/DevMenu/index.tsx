import styled from 'styled-components';

import { Link } from '../ui';
import { ROUTES } from '../../routes';
import { IS_DEV } from '../../utils/constants/env';

const StyledMenu = styled.ul`
    position: fixed;
    right: 0;
    top: 0;
    padding: 0;
    margin: 0;
    display: flex;
    border: orange 2px solid;
    background: #36393f;

    li {
        display: inline-block;

        a {
            display: inline-block;
            padding: 10px 20px;
            transition: 0.3s;

            &:hover {
                background-color: #000;
            }
        }
    }
`;

const routes = Object.values(ROUTES);

export function DevMenu() {
    if (IS_DEV) {
        return (
            <StyledMenu>
                {routes.map((it) => (
                    <li key={it.title}>
                        <Link to={it.path}>{it.title}</Link>
                    </li>
                ))}
            </StyledMenu>
        );
    }

    return null;
}
