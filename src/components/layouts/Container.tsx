import styled from 'styled-components';

export const Container = styled.div`
    max-width: 860px;
    margin: 0 auto;
    text-align: center;
    padding-bottom: 70px;
`;
