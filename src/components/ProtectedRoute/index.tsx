import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { ROUTES } from '../../routes';
import { TRootState } from '../../store';

interface IProtectedRouteComponentProps extends RouteProps {
    isAuthenticated: boolean;
}

export function ProtectedRouteComponent({ component, isAuthenticated, ...rest }: IProtectedRouteComponentProps) {
    return (
        <Route
            {...rest}
            render={(props) =>
                isAuthenticated ? (
                    <Route {...props} {...rest}>
                        {component}
                    </Route>
                ) : (
                    <Redirect
                        to={{
                            pathname: ROUTES.LOGIN.path,
                            state: {
                                from: props.location,
                            },
                        }}
                    />
                )
            }
        />
    );
}

export const ProtectedRoute = connect((store: TRootState) => ({ isAuthenticated: store.auth.isAuthenticated }))(
    ProtectedRouteComponent
);
