export interface IProps {
    children: React.ReactNode;
    to: string;
    className?: string;
}
