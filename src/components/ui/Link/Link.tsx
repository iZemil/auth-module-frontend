import { IProps } from './types';
import { StyledLink } from './Styled';

export function Link({ className, children, to }: IProps) {
    return (
        <StyledLink className={className} to={to}>
            {children}
        </StyledLink>
    );
}
