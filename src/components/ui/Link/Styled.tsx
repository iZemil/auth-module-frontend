import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { COLORS } from '../../../utils/constants/colors';

export const StyledLink = styled(Link)`
    font-size: 14px;
    line-height: 16px;
    text-align: left;
    color: ${COLORS.primary};
    font-weight: 500;
    text-decoration: none;
`;
