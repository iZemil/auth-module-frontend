import { IProps } from './types';
import { StyledButton } from './Styled';

export function Button({ type, onClick, children }: IProps) {
    return (
        <StyledButton type={type} onClick={onClick}>
            {children}
        </StyledButton>
    );
}
