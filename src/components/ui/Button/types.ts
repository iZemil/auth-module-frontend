export interface IProps {
    type: 'button' | 'submit' | 'reset' | undefined;
    children: React.ReactNode;
    onClick?: () => void;
}
