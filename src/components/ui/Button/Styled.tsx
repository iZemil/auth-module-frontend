import styled from 'styled-components';

import { COLORS } from '../../../utils/constants/colors';

export const StyledButton = styled.button`
    font-size: 16px;
    line-height: 24px;
    color: #fff;
    background-color: ${COLORS.primary};
    height: 44px;
    min-width: 130px;
    min-height: 44px;
    transition: background-color 0.17s ease, color 0.17s ease;
    border: none;
    border-radius: 3px;
    padding: 2px 16px;
    cursor: pointer;
`;
