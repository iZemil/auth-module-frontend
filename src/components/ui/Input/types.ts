import { FieldError } from 'react-hook-form';

export interface IProps {
    label: string;
    name: string;
    register: (ref: HTMLInputElement) => void;
    onChange?: (e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => void;
    defaultValue?: string;
    type?: 'password' | 'text' | 'email';
    className?: string;
    err?: FieldError;
}

export interface IInputAreaProps {
    err: IProps['err'];
}
