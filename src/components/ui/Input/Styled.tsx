import styled from 'styled-components';

import { COLORS } from '../../../utils/constants/colors';
import { IInputAreaProps } from './types';

export const InputWrapper = styled.div`
    margin-bottom: 20px;
`;

export const InputLabel = styled.label`
    display: block;
    padding: 0 0 4px;
    font-size: 14px;
    font-weight: 500;
    line-height: 16px;
    color: ${COLORS.secondaryText};
    text-align: left;
`;

export const InputArea = styled.input<IInputAreaProps>`
    padding: 10px;
    height: 40px;
    font-size: 16px;
    box-sizing: border-box;
    width: 100%;
    border-radius: 3px;
    color: ${COLORS.primaryText};
    background-color: transparent;
    border: 1px solid ${(props) => (props.err ? COLORS.error : COLORS.black)};
    transition: border-color 0.2s ease-in-out;
`;
