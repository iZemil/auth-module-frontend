import { IProps } from './types';
import { InputArea, InputLabel, InputWrapper } from './Styled';

export function Input({ className, type, defaultValue, label, name, register, onChange, err }: IProps) {
    return (
        <InputWrapper>
            <InputLabel htmlFor={label}>{label}</InputLabel>
            <InputArea
                id={label}
                defaultValue={defaultValue}
                type={type}
                className={className}
                name={name}
                ref={register}
                onChange={onChange}
                err={err}
            />
        </InputWrapper>
    );
}
