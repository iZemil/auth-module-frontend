const express = require('express');
const bodyParser = require('body-parser');

const AuthRouter = require('./routes');

const FRONTEND_ORIGIN = 'http://localhost:3000';
const AUTH_PORT = 8082;

const SecurityApp = express();

const cors = (req, res, next) => {
    const allowedOrigins = [FRONTEND_ORIGIN];
    const { origin } = req.headers;

    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }

    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Expose-Headers', 'Content-Length,Content-Range');
    res.header('Access-Control-Allow-Credentials', true);
    next();
};

SecurityApp.use(bodyParser.urlencoded({ extended: false }));
SecurityApp.use(bodyParser.json());
SecurityApp.use(cors);

SecurityApp.use('/api/auth', AuthRouter);

SecurityApp.listen(AUTH_PORT, () => {
    console.log(`Security mock server is running: http://localhost:${AUTH_PORT}`);
});
