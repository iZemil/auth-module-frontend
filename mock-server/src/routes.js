const express = require('express');

const RESPONSE_DELAY = 1000;
const ALLOWED_USERS = ['admin@admin.com', 'admin@mail.com', 'admin@gmail.com'];

const AuthRouter = express.Router();
let userData = null;

AuthRouter.get('/', (req, res) => {
    res.send('<b>Auth API is working!</b>');
});

AuthRouter.post('/sign-up', (req, res) => {
    setTimeout(() => {
        const { email, username } = req.body;
        userData = {
            email,
            username,
        };

        res.status(200);
        res.json(userData);
    }, RESPONSE_DELAY);
});

AuthRouter.get('/info/:email').post('/login', (req, res) => {
    setTimeout(() => {
        let { email } = req.body;

        if (req.params.email) {
            email = req.params.email;
        }

        if (ALLOWED_USERS.includes(email)) {
            userData = {
                email,
                username: userData ? userData.username : 'admin',
            };
            res.status(200);
            res.json(userData);
        } else {
            res.status(403);
            res.json({
                code: 'FORBIDDEN',
                message: 'Неверный логин и/или пароль',
                error: true,
            });
        }
    }, RESPONSE_DELAY);
});

AuthRouter.post('/sign-out', (req, res) => {
    setTimeout(() => {
        userData = null;
        res.sendStatus(200);
    }, RESPONSE_DELAY);
});

module.exports = AuthRouter;
