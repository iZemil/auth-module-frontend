# Form Frontend Application

## Commands:

-   run development `npm start` with mock-server `npm run dev:mock`
-   create production build `npm run build`
-   run container `docker-compose -f docker-compose.yml up -d --build`
-   stop container `docker-compose stop`

### Envs:

-   define API url `process.env.API_URL`

### NOTES:

-   Login with 'admin@admin.com', 'admin@mail.com', 'admin@gmail.com' and Any password
